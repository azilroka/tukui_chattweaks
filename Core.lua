if not (IsAddOnLoaded("Tukui") or IsAddOnLoaded("AsphyxiaUI") or IsAddOnLoaded("DuffedUI")) then return end
local A, C, L = unpack(Tukui or AsphyxiaUI or DuffedUI)
ChatTweaksOptions = {}

local ChatBackgroundRight = A.Panels.RightChatBackground
local ChatBackgroundLeft = A.Panels.LeftChatBackground
local InfoLeft = A.Panels.DataTextLeft
local InfoRight = A.Panels.DataTextRight
local TabsRightBackground = A.Panels.RightChatTabsBackground
local TabsLeftBackground = A.Panels.LeftChatTabsBackground
local ExitVehicleLeft = A.Panels.VehicleButtonLeft
local ExitVehicleRight = A.Panels.VehicleButtonRight

local UIMinimap = A.Minimap
local Blank = C["Media"].Blank
local BorderColor = C["Media"].BorderColor
local Font = C["Media"].Font
local ActionBarFont = C["Media"].ActionBarFont
local NormTex = C["Media"].Normal
local ChatEnable = C["Chat"].Enable
local MyClass = select(2, UnitClass("player"))

local TopPanel = CreateFrame("Frame", "TopPanel", UIParent)
TopPanel:SetPoint("TOP", UIParent, 0, 4)
TopPanel:SetTemplate("Transparent")
TopPanel:SetFrameStrata("BACKGROUND")
TopPanel:SetFrameLevel(0)

local BottomPanel = CreateFrame("Frame", "BottomPanel", UIParent)
BottomPanel:SetPoint("BOTTOM", UIParent, 0, -4)
BottomPanel:SetTemplate("Transparent")
BottomPanel:SetFrameStrata("BACKGROUND")
BottomPanel:SetFrameLevel(0)

local function ApplyChatBackground(name, option1, option2)
	local Texture
	if ChatTweaksOptions[option2] then
		if MyClass == "DEATHKNIGHT" then Texture = ChatTweaksOptions["ChatBackgroundPath"].."dk.tga" end
		if MyClass == "DRUID" then Texture = ChatTweaksOptions["ChatBackgroundPath"].."druid.tga" end
		if MyClass == "HUNTER" then Texture = ChatTweaksOptions["ChatBackgroundPath"].."hunter.tga" end
		if MyClass == "MAGE" then Texture = ChatTweaksOptions["ChatBackgroundPath"].."mage.tga" end
		if MyClass == "MONK" then Texture = ChatTweaksOptions["ChatBackgroundPath"].."monk.tga" end
		if MyClass == "PALADIN" then Texture = ChatTweaksOptions["ChatBackgroundPath"].."paladin.tga" end
		if MyClass == "PRIEST" then Texture = ChatTweaksOptions["ChatBackgroundPath"].."priest.tga" end
		if MyClass == "ROGUE" then Texture = ChatTweaksOptions["ChatBackgroundPath"].."rogue.tga" end
		if MyClass == "SHAMAN" then Texture = ChatTweaksOptions["ChatBackgroundPath"].."shaman.tga" end
		if MyClass == "WARLOCK" then Texture = ChatTweaksOptions["ChatBackgroundPath"].."warlock.tga" end
		if MyClass == "WARRIOR" then Texture = ChatTweaksOptions["ChatBackgroundPath"].."warrior.tga" end
	else
		Texture = ChatTweaksOptions["ChatBackgroundPath"]..ChatTweaksOptions[option1]
	end
	if not DuffedUI then
		if name == ChatBackgroundLeft then InfoLeft:StripTextures() end
		if name == ChatBackgroundRight then InfoRight:StripTextures() end
	end
	if name == ChatBackgroundLeft then TabsLeftBackground:StripTextures() end
	if name == ChatBackgroundRight then TabsRightBackground:StripTextures() end
	name:SetBackdrop({
		bgFile = Texture, 
		edgeFile = Blank, 
		tile = false, tileSize = 0, edgeSize = A.mult,
	})
	name:SetBackdropBorderColor(unpack(BorderColor))
end

local function ChatPanelResize()
	if ChatBackgroundRight and not DuffedUI then
		ChatBackgroundRight:Size(ChatTweaksOptions["ChatWidth"], ChatTweaksOptions["ChatHeight"])
		ChatBackgroundLeft:Size(ChatTweaksOptions["ChatWidth"], ChatTweaksOptions["ChatHeight"])
		InfoRight:Width(ChatBackgroundRight:GetWidth() - 10)
		InfoLeft:Width(ChatBackgroundRight:GetWidth() - 10)
		TabsLeftBackground:Width(ChatBackgroundRight:GetWidth() - 10)
		TabsRightBackground:Width(ChatBackgroundRight:GetWidth() - 10)
		GeneralDockManager:Size(ChatTweaksOptions["ChatWidth"], InfoLeft:GetHeight())
	end
end

local len, gsub, find, sub, gmatch, format, random = string.len, string.gsub, string.find, string.sub, string.gmatch, string.format, math.random

local smileyPack = {
	["Angry"] = [[Interface\AddOns\ElvUI\media\textures\smileys\angry.blp]],
	["Grin"] = [[Interface\AddOns\ElvUI\media\textures\smileys\grin.blp]],
	["Hmm"] = [[Interface\AddOns\ElvUI\media\textures\smileys\hmm.blp]],
	["MiddleFinger"] = [[Interface\AddOns\ElvUI\media\textures\smileys\middle_finger.blp]],
	["Sad"] = [[Interface\AddOns\ElvUI\media\textures\smileys\sad.blp]],
	["Surprise"] = [[Interface\AddOns\ElvUI\media\textures\smileys\surprise.blp]],
	["Tongue"] = [[Interface\AddOns\ElvUI\media\textures\smileys\tongue.blp]],
	["Cry"] = [[Interface\AddOns\ElvUI\media\textures\smileys\weepy.blp]],
	["Wink"] = [[Interface\AddOns\ElvUI\media\textures\smileys\winky.blp]],
	["Happy"] = [[Interface\AddOns\ElvUI\media\textures\smileys\happy.blp]],
	["Heart"] = [[Interface\AddOns\ElvUI\media\textures\smileys\heart.blp]],
	['BrokenHeart'] = [[Interface\AddOns\ElvUI\media\textures\smileys\broken_heart.blp]],
}

local smileyKeys = {
	["%:%-%@"] = "Angry",
	["%:%@"] = "Angry",
	["%:%-%)"] = "Happy",
	["%:%)"] = "Happy",
	["%:D"] = "Grin",
	["%:%-D"] = "Grin",
	["%;%-D"] = "Grin",
	["%;D"] = "Grin",
	["%=D"] = "Grin",
	["xD"] = "Grin",
	["XD"] = "Grin",
	["%:%-%("] = "Sad",
	["%:%("] = "Sad",
	["%:o"] = "Surprise",
	["%:%-o"] = "Surprise",
	["%:%-O"] = "Surprise",
	["%:O"] = "Surprise",
	["%:%-0"] = "Surprise",
	["%:P"] = "Tongue",
	["%:%-P"] = "Tongue",
	["%:p"] = "Tongue",
	["%:%-p"] = "Tongue",
	["%=P"] = "Tongue",
	["%=p"] = "Tongue",
	["%;%-p"] = "Tongue",
	["%;p"] = "Tongue",
	["%;P"] = "Tongue",
	["%;%-P"] = "Tongue",
	["%;%-%)"] = "Wink",
	["%;%)"] = "Wink",
	["%:S"] = "Hmm",
	["%:%-S"] = "Hmm",
	["%:%,%("] = "Cry",
	["%:%,%-%("] = "Cry",
	["%:%'%("] = "Cry",
	["%:%'%-%("] = "Cry",
	["%:%F"] = "MiddleFinger",
	["<3"] = "Heart",
	["</3"] = "BrokenHeart",
}

local function InsertEmotions(self, event, msg)
	for k,v in pairs(smileyKeys) do
		msg = gsub(msg,k,"|T"..smileyPack[v]..":16|t")
	end
	return msg
end

local function GetSmileyReplacementText(self, event, msg)
	if not msg then return end
	if msg:find('/run') or msg:find('/dump') or msg:find('/script') then return msg end
	local outstr = ""
	local origlen = len(msg)
	local startpos = 1
	local endpos
	
	while startpos <= origlen do
		endpos = origlen
		local pos = find(msg,"|H",startpos,true)
		if pos ~= nil then
			endpos = pos
		end
		outstr = outstr .. InsertEmotions(sub(msg,startpos,endpos))
		startpos = endpos + 1
		if pos ~= nil then
			endpos = find(msg,"|h]|r",startpos,-1) or find(msg,"|h",startpos,-1)
			endpos = endpos or origlen
			if(startpos < endpos) then
				outstr = outstr .. sub(msg,startpos,endpos)
				startpos = endpos + 1
			end
		end
	end
	
	return outstr
end

local function LootIcons(self, event, message, ...)
	local _, size = GetChatWindowInfo(self:GetID())
	local function IconForLink(link)
		local tex = GetItemIcon(link)
		return "\124T"..tex..":"..size.."\124t"..link
	end
	message = message:gsub("(\124c%x+\124Hitem:.-\124h\124r)", IconForLink)
	return false, message, ...
end

local function EnableLootIcons()
	ChatFrame_AddMessageEventFilter("CHAT_MSG_LOOT", LootIcons)
end

local function DisableLootIcons()
	ChatFrame_RemoveMessageEventFilter("CHAT_MSG_LOOT", LootIcons)
end

local function ApplyBackgrounds()
	ApplyChatBackground(ChatBackgroundLeft, "LeftChatBackground", "LeftClassChatBackground")
	ApplyChatBackground(ChatBackgroundRight, "RightChatBackground", "RightClassChatBackground")
end

local function Disable(name)
	name:Hide()
	if name.text then name.text:Hide() end
end

local function SetupParents()
	for i = 1, NUM_CHAT_WINDOWS do
		chat = _G[format("ChatFrame%d", i)]
		tab = _G[format("ChatFrame%sTab", i)]
		if tab:GetParent() == UIParent then
			chat:SetParent(tab)
			tab:SetParent(i == 4 and ChatBackgroundRight or ChatBackgroundLeft)
			chat:ClearAllPoints()
			chat:SetPoint("TOPLEFT", i == 4 and TabsRightBackground or TabsLeftBackground, "BOTTOMLEFT", 0, -1)
			chat:SetPoint("BOTTOMRIGHT", i == 4 and InfoRight or InfoLeft, "TOPRIGHT", 0, 1)
			if DuffedUI then chat:SetPoint("BOTTOMRIGHT", i == 4 and ChatBackgroundRight or ChatBackgroundLeft, "BOTTOMRIGHT", 0, 1) end
		end
	end
end

hooksecurefunc("FCF_OpenTemporaryWindow", SetupParents)

local ChatTweaksOptionsFrame = CreateFrame("Frame", "ChatTweaksOptionsFrame", UIParent)
ChatTweaksOptionsFrame:Hide()
ChatTweaksOptionsFrame:SetTemplate("Transparent")
ChatTweaksOptionsFrame:Width(350)
ChatTweaksOptionsFrame:Height(450)
ChatTweaksOptionsFrame:Point("CENTER", UIParent, "CENTER", 0, 0)
ChatTweaksOptionsFrame:SetClampedToScreen(true)
ChatTweaksOptionsFrame:SetMovable(true)
ChatTweaksOptionsFrame:EnableMouse(true)
ChatTweaksOptionsFrame:RegisterForDrag("LeftButton")
ChatTweaksOptionsFrame:SetScript("OnDragStart", function(self) self:StartMoving() end)
ChatTweaksOptionsFrame:SetScript("OnDragStop", function(self) self:StopMovingOrSizing()  end)
ChatTweaksOptionsFrame:FontString("text", Font, 14, "OUTLINE")
ChatTweaksOptionsFrame.text:SetPoint("TOP", ChatTweaksOptionsFrame, 0, -6)
ChatTweaksOptionsFrame.text:SetText("|cffC495DDTukui|r ChatTweaks Options")
ChatTweaksOptionsFrame:SetScript("OnShow", function(self)
	if not ChatBackgroundLeft then
		Disable(ChatHiderButton)
		Disable(LeftChatBackgroundEditBox)
		Disable(RightChatBackgroundEditBox)
		Disable(ChatBackgroundButton)
		Disable(ClassChatBackgroundRightButton)
		Disable(ClassChatBackgroundLeftButton)
		Disable(ClassChatBackgroundEditBox)
	end
	if ChatBackgroundLeft and not ChatTweaksOptions["ChatBackground"] then
		Disable(LeftChatBackgroundEditBox)
		Disable(RightChatBackgroundEditBox)
	end
	if not ChatBackgroundRight or DuffedUI then
		Disable(ChatHeightSlider)
		Disable(ChatWidthSlider)
		Disable(ChatHeightEditBox)
		Disable(ChatWidthEditBox)
	end
end)

local ChatTweaksOptionsFrameCloseButton = CreateFrame("Button", "ChatTweaksOptionsFrameCloseButton", ChatTweaksOptionsFrame, "UIPanelButtonTemplate")
ChatTweaksOptionsFrameCloseButton:SetPoint("TOP", ChatTweaksOptionsFrame, "BOTTOM", 0, -2)
ChatTweaksOptionsFrameCloseButton:Size(ChatTweaksOptionsFrame:GetWidth(),24)
ChatTweaksOptionsFrameCloseButton:SkinButton()
ChatTweaksOptionsFrameCloseButton:FontString("text", Font, 12, "OUTLINE")
ChatTweaksOptionsFrameCloseButton.text:SetPoint("CENTER", ChatTweaksOptionsFrameCloseButton, 0, 0)
ChatTweaksOptionsFrameCloseButton.text:SetText("Close Options")
ChatTweaksOptionsFrameCloseButton:HookScript("OnClick", function()
	ChatTweaksOptionsFrame:Hide()
end)

local function CreateButton(name, text)
	local frame = CreateFrame("Button", name.."Button", ChatTweaksOptionsFrame)
	frame:SetSize(16, 16)
	frame:CreateBackdrop()
	frame:SetBackdrop({bgFile = NormTex, tile = false, tileSize = 0})
	frame:FontString("text", Font, 12, "OUTLINE")
	frame.text:SetText(text)
	frame:SetScript("OnShow", function(self)
		if ChatTweaksOptions[name] then
			self:SetBackdropColor(.11, .66, .11, 1)
		else
			self:SetBackdropColor(.68, .14, .14, 1)
		end
	end)
	frame:SetScript("PostClick", function(self)
		if ChatTweaksOptions[name] then
			self:SetBackdropColor(.11, .66, .11, 1)
		else
			self:SetBackdropColor(.68, .14, .14, 1)
		end
	end)
end

CreateButton("LootIcons", "Loot Icons")
LootIconsButton:SetPoint("TOPLEFT", ChatTweaksOptionsFrame, "TOPLEFT", 12, -40)
LootIconsButton.text:SetPoint("LEFT", LootIconsButton, "RIGHT", 12, 0)
LootIconsButton:SetScript("OnClick", function()
	if ChatTweaksOptions["LootIcons"] then
		ChatTweaksOptions["LootIcons"] = false
		DisableLootIcons()
	else
		ChatTweaksOptions["LootIcons"] = true
		EnableLootIcons()
	end
end)

CreateButton("ChatHider", "Able to Hide Chat Panels")
ChatHiderButton:SetPoint("TOPLEFT", ChatTweaksOptionsFrame, "TOPLEFT", 12, -64)
ChatHiderButton.text:SetPoint("LEFT", ChatHiderButton, "RIGHT", 12, 0)
ChatHiderButton:SetScript("OnClick", function()
	if ChatTweaksOptions["ChatHider"] then
		ChatTweaksOptions["ChatHider"] = false
		if InfoRight.Faded then
			InfoRight.Faded = nil
			UIFrameFadeIn(InfoRight, 0.2, InfoRight:GetAlpha(), 1)
			UIFrameFadeIn(ChatBackgroundRight, 0.2, ChatBackgroundRight:GetAlpha(), 1)
		end
		if InfoLeft.Faded then
			InfoLeft.Faded = nil
			UIFrameFadeIn(InfoLeft, 0.2, InfoLeft:GetAlpha(), 1)
			UIFrameFadeIn(GeneralDockManager, 0.2, GeneralDockManager:GetAlpha(), 1)
			UIFrameFadeIn(ChatBackgroundLeft, 0.2, ChatBackgroundLeft:GetAlpha(), 1)
		end
	else
		ChatTweaksOptions["ChatHider"] = true
	end
end)

CreateButton("ChatBackground", "Chat Background")
ChatBackgroundButton:SetPoint("BOTTOM", ChatTweaksOptionsFrame, -80, 150)
ChatBackgroundButton.text:SetPoint("LEFT", ChatBackgroundButton, "RIGHT", 12, 0)
ChatBackgroundButton:SetScript("OnClick", function()
	if ChatTweaksOptions["ChatBackground"] then
		ChatTweaksOptions["ChatBackground"] = false
		LeftChatBackgroundEditBox:Disable()
		RightChatBackgroundEditBox:Disable()
		ClassChatBackgroundRightButton:Disable()
		ClassChatBackgroundLeftButton:Disable()
		ClassChatBackgroundEditBox:Disable()
	else
		ChatTweaksOptions["ChatBackground"] = true
		LeftChatBackgroundEditBox:Enable()
		RightChatBackgroundEditBox:Enable()
		ClassChatBackgroundRightButton:Enable()
		ClassChatBackgroundLeftButton:Enable()
		ClassChatBackgroundEditBox:Enable()
	end
end)

local function CreateEditBox(name, text)
	local frame = CreateFrame("EditBox", name.."EditBox", ChatTweaksOptionsFrame)
	frame:FontString("text", Font, 12, "OUTLINE")
	frame.text:SetText(text)
	frame:SetAutoFocus(false)
	frame:SetMultiLine(false)
	frame:SetWidth(140)
	frame:SetHeight(20)
	frame:SetMaxLetters(255)
	frame:SetTextInsets(3, 0, 0, 0)
	frame:SetFontObject(GameFontHighlight)
	frame:SetTemplate("Default")
	frame:SetScript("OnShow", function(self) self:SetText(ChatTweaksOptions[name]) end)
	frame:SetScript("OnEscapePressed", function(self) self:ClearFocus() self:SetText(ChatTweaksOptions[name]) end)
	frame:SetScript("OnEnterPressed", function(self)
		self:ClearFocus()
		ChatTweaksOptions[name] = self:GetText()
		self:SetText(ChatTweaksOptions[name])
		ApplyBackgrounds()
	end)
end

CreateEditBox("LeftChatBackground", "Left Chat Background")
LeftChatBackgroundEditBox:SetPoint("BOTTOMLEFT", ChatTweaksOptionsFrame, 12, 60)
LeftChatBackgroundEditBox.text:SetPoint("BOTTOMLEFT", LeftChatBackgroundEditBox, "TOPLEFT", 0, 2)

CreateEditBox("RightChatBackground", "Right Chat Background")
RightChatBackgroundEditBox.text:SetPoint("BOTTOMRIGHT", RightChatBackgroundEditBox, "TOPRIGHT", 0, 2)
RightChatBackgroundEditBox:SetPoint("BOTTOMRIGHT", ChatTweaksOptionsFrame, -12, 60)

CreateEditBox("ClassChatBackground", "Chat Background Path")
ClassChatBackgroundEditBox.text:SetPoint("BOTTOM", ClassChatBackgroundEditBox, "TOP", 0, 2)
ClassChatBackgroundEditBox:SetPoint("BOTTOM", ChatTweaksOptionsFrame, 0, 100)
ClassChatBackgroundEditBox:SetWidth(280)

CreateButton("LeftClassChatBackground", "Class")
LeftClassChatBackgroundButton:SetPoint("TOPLEFT", LeftChatBackgroundEditBox, "BOTTOMLEFT", 2, -5)
LeftClassChatBackgroundButton.text:SetPoint("LEFT", LeftClassChatBackgroundButton, "RIGHT", 12, 0)
LeftClassChatBackgroundButton:SetScript("OnClick", function()
	if ChatTweaksOptions["LeftClassChatBackground"] then
		ChatTweaksOptions["LeftClassChatBackground"] = false
		LeftChatBackgroundEditBox:Enable()
	else
		ChatTweaksOptions["LeftClassChatBackground"] = true
		LeftChatBackgroundEditBox:Disable()
	end
	ApplyBackgrounds()
end)

CreateButton("RightClassChatBackground", "Class")
RightClassChatBackgroundButton:SetPoint("TOPRIGHT", RightChatBackgroundEditBox, "BOTTOMRIGHT", -2, -5)
RightClassChatBackgroundButton.text:SetPoint("RIGHT", RightClassChatBackgroundButton, "LEFT", -12, 0)
RightClassChatBackgroundButton:SetScript("OnClick", function()
	if ChatTweaksOptions["RightClassChatBackground"] then
		ChatTweaksOptions["RightClassChatBackground"] = false
		RightChatBackgroundEditBox:Enable()
	else
		ChatTweaksOptions["RightClassChatBackground"] = true
		RightChatBackgroundEditBox:Disable()
	end
	ApplyBackgrounds()
end)

local function CreateSlider(name, text, min, max)
	local frame = CreateFrame("Slider", name.."Slider", ChatTweaksOptionsFrame, "OptionsSliderTemplate")
	local frame2 = CreateFrame("EditBox", name.."EditBox", frame)
	frame:SetSize(100, 15)
	frame:SetOrientation("HORIZONTAL")
	frame:SetMinMaxValues(min, max)
	frame:SetValueStep(1)
	_G[frame:GetName().."Low"]:SetText(min)
	_G[frame:GetName().."High"]:SetText(max)
	_G[frame:GetName().."Text"]:SetText(text)
	frame:SetScript("OnShow", function(self) self:SetValue(ChatTweaksOptions[name]) end)
	frame:SetScript("OnValueChanged", function(self, value)
		ChatTweaksOptions[name] = self:GetValue(value)
		self:SetValue(ChatTweaksOptions[name])
		frame2:SetText(ChatTweaksOptions[name])
	end)
	frame:SkinSlideBar(10, true)

	frame2:SetTemplate("Default")
	frame2:SetAutoFocus(false)
	frame2:SetMultiLine(false)
	frame2:SetWidth(30)
	frame2:SetHeight(20)
	frame2:SetMaxLetters(3)
	frame2:SetTextInsets(3, 0, 0, 0)
	frame2:SetFontObject(GameFontHighlight)
	frame2:SetPoint("TOP", frame, "BOTTOM", 0, -2)
	frame2:SetScript("OnShow", function(self) self:SetText(ChatTweaksOptions[name]) end)
	frame2:SetScript("OnEscapePressed", function(self) self:ClearFocus() self:SetText(ChatTweaksOptions[name]) end)
	frame2:SetScript("OnEnterPressed", function(self)
		self:ClearFocus()
		ChatTweaksOptions[name] = self:GetText()
		frame:SetValue(ChatTweaksOptions[name])
	end)
end

CreateSlider("ChatHeight", "Chat Height", 100, 600)
ChatHeightSlider:SetPoint("TOPRIGHT", ChatTweaksOptionsFrame, "TOPRIGHT", -15, -50)
ChatHeightSlider:HookScript("OnValueChanged", ChatPanelResize)

CreateSlider("ChatWidth", "Chat Width", 100, 600)
ChatWidthSlider:SetPoint("TOPRIGHT", ChatTweaksOptionsFrame, "TOPRIGHT", -15, -110)
ChatWidthSlider:HookScript("OnValueChanged", ChatPanelResize)

CreateSlider("TopPanelHeight", "Top Panel Height", 0, 100)
TopPanelHeightSlider:SetPoint("TOPRIGHT", ChatTweaksOptionsFrame, "TOPRIGHT", -15, -160)
TopPanelHeightSlider:HookScript("OnValueChanged", function(self) TopPanel:Size(UIParent:GetWidth() + 10, ChatTweaksOptions["TopPanelHeight"] + 4) end)

CreateSlider("BottomPanelHeight", "Bottom Panel Height", 0, 100)
BottomPanelHeightSlider:SetPoint("TOPRIGHT", ChatTweaksOptionsFrame, "TOPRIGHT", -15, -210)
BottomPanelHeightSlider:HookScript("OnValueChanged", function(self) BottomPanel:Size(UIParent:GetWidth() + 10, ChatTweaksOptions["BottomPanelHeight"] + 4) end)

local LoadChatTweaks = CreateFrame("Frame")
LoadChatTweaks:RegisterEvent("PLAYER_ENTERING_WORLD")
LoadChatTweaks:SetScript("OnEvent", function(self, event)
	self:UnregisterEvent(event)
	if ChatTweaksOptions["LootIcons"] == nil then ChatTweaksOptions["LootIcons"] = true end
	if ChatTweaksOptions["ChatHider"] == nil then ChatTweaksOptions["ChatHider"] = true end
	if ChatTweaksOptions["ChatBackground"] == nil then ChatTweaksOptions["ChatBackground"] = false end
	if ChatTweaksOptions["LeftChatBackground"] == nil then ChatTweaksOptions["LeftChatBackground"] = "horde.tga" end
	if ChatTweaksOptions["RightChatBackground"] == nil then ChatTweaksOptions["RightChatBackground"] = "alliance.tga" end
	if ChatTweaksOptions["ChatBackgroundPath"] == nil then ChatTweaksOptions["ChatBackgroundPath"] = "Interface\\AddOns\\ChatTextures\\" end
	if ChatTweaksOptions["LeftClassChatBackground"] == nil then ChatTweaksOptions["LeftClassChatBackground"] = false end
	if ChatTweaksOptions["RightClassChatBackground"] == nil then ChatTweaksOptions["RightClassChatBackground"] = false end
	if ChatTweaksOptions["ChatResizer"] == nil then ChatTweaksOptions["ChatResizer"] = true end
	if ChatTweaksOptions["ChatHeight"] == nil then ChatTweaksOptions["ChatHeight"] = 175 end
	if ChatTweaksOptions["ChatWidth"] == nil then ChatTweaksOptions["ChatWidth"] = 378 end
	if ChatTweaksOptions["TopPanelHeight"] == nil then ChatTweaksOptions["TopPanelHeight"] = 30 end
	if ChatTweaksOptions["BottomPanelHeight"] == nil then ChatTweaksOptions["BottomPanelHeight"] = 30 end

	if ChatBackgroundLeft and ChatEnable then
		SetupParents()
		GeneralDockManager:ClearAllPoints()
		GeneralDockManager:SetPoint("TOP", ChatBackgroundLeft, "TOP", 0, -2)
		GeneralDockManager:Size(ChatTweaksOptions["ChatWidth"], InfoLeft:GetHeight())
		if TukuiLineToABLeft then TukuiLineToABLeft:Hide() TukuiLineToABLeftAlt:Hide() TukuiLineToABRight:Hide() TukuiLineToABRightAlt:Hide() end
	end

	if ChatBackgroundLeft and ChatTweaksOptions["ChatBackground"] then
		ApplyBackgrounds()
	end

	if ChatBackgroundLeft and ChatTweaksOptions["ChatResizer"] then
		ChatTweaksChatPanelResize()
	end

	if ChatTweaksOptions["LootIcons"] then
		EnableLootIcons()
	end

	TopPanel:Size(UIParent:GetWidth() + 10, ChatTweaksOptions["TopPanelHeight"] + 4)
	BottomPanel:Size(UIParent:GetWidth() + 10, ChatTweaksOptions["BottomPanelHeight"] + 4)

	if Tukui then
		local LeftTukuiExitVehicleButton = CreateFrame("Frame", "LeftTukuiExitVehicleButton", UIParent)
		LeftTukuiExitVehicleButton:SetAllPoints(ExitVehicleLeft)
		LeftTukuiExitVehicleButton:ClearAllPoints()
		ExitVehicleLeft:SetParent(LeftTukuiExitVehicleButton)
		ExitVehicleLeft:SetAllPoints(LeftTukuiExitVehicleButton)

		local RightTukuiExitVehicleButton = CreateFrame("Frame", "RightTukuiExitVehicleButton", UIParent)
		RightTukuiExitVehicleButton:SetAllPoints(ExitVehicleRight)
		RightTukuiExitVehicleButton:ClearAllPoints()
		ExitVehicleRight:SetParent(RightTukuiExitVehicleButton)
		ExitVehicleRight:SetAllPoints(RightTukuiExitVehicleButton)
	end
end)

if not IsAddOnLoaded("Tukui_Skins") and ChatBackgroundRight then
	ChatBackgroundRight:SetFrameStrata("Background")
	ChatBackgroundLeft:SetFrameStrata("Background")
	TabsRightBackground:SetParent(ChatBackgroundRight)
	TabsLeftBackground:SetParent(ChatBackgroundLeft)

	local function CreateToggleButton(name, buttontext, panel1, panel2, tooltiptext)
		local frame = CreateFrame("Button", name, UIParent)
		frame:SetTemplate("Transparent")
		frame:Size(panel1:GetHeight()-4)
		frame:FontString("text", ActionBarFont, 12)
		frame.text:SetText(buttontext)
		frame.text:SetPoint("CENTER", 2, 2)
		frame:RegisterForClicks("LeftButtonDown", "RightButtonDown");
		UIFrameFadeOut(frame, 0.2, frame:GetAlpha(), 0)
		frame:SetScript("OnClick", function(self, btn)
			if not ChatTweaksOptions["ChatHider"] then return end
			if panel2 then
				if self.Faded then
					self.Faded = nil
					if not DuffedUI then UIFrameFadeIn(panel1, 0.2, panel1:GetAlpha(), 1) end
					UIFrameFadeIn(panel2, 0.2, panel2:GetAlpha(), 1)
					if name == "LeftToggleButton" then UIFrameFadeIn(GeneralDockManager, 0.2, GeneralDockManager:GetAlpha(), 1) end
				else
					self.Faded = true
					if not DuffedUI then UIFrameFadeOut(panel1, 0.2, panel1:GetAlpha(), 0) end
					UIFrameFadeOut(panel2, 0.2, panel2:GetAlpha(), 0)
					if name == "LeftToggleButton" then UIFrameFadeOut(GeneralDockManager, 0.2, GeneralDockManager:GetAlpha(), 0) end
				end
			end
		end)
		frame:SetScript("OnEnter", function(self, ...)
			if not ChatTweaksOptions["ChatHider"] then return end
			UIFrameFadeIn(self, 0.2, self:GetAlpha(), 1)
			if self.Faded then
				if not DuffedUI then UIFrameFadeIn(panel1, 0.2, panel1:GetAlpha(), 1) end
				UIFrameFadeIn(panel2, 0.2, panel2:GetAlpha(), 1)
				if name == "LeftToggleButton" then UIFrameFadeIn(GeneralDockManager, 0.2, GeneralDockManager:GetAlpha(), 1) end
			end
			GameTooltip:SetOwner(self, name == "LeftToggleButton" and "ANCHOR_TOPLEFT" or "ANCHOR_TOPRIGHT", 0, 4)
			GameTooltip:ClearLines()
			GameTooltip:AddDoubleLine("Left Click:", tooltiptext, 1, 1, 1)
			GameTooltip:Show()
		end)
		frame:SetScript("OnLeave", function(self, ...)
			if not ChatTweaksOptions["ChatHider"] then return end
			if self.Faded then
				if not DuffedUI then UIFrameFadeOut(panel1, 0.2, panel1:GetAlpha(), 0) end
				UIFrameFadeOut(panel2, 0.2, panel2:GetAlpha(), 0)
				if name == "LeftToggleButton" then UIFrameFadeOut(GeneralDockManager, 0.2, GeneralDockManager:GetAlpha(), 0) end
			end
			UIFrameFadeOut(self, 0.2, self:GetAlpha(), 0)
			GameTooltip:Hide()
		end)
	end

	CreateToggleButton("RightToggleButton", "►", InfoRight, ChatBackgroundRight, "Toggle Right Chat Panel")
	RightToggleButton:Point("RIGHT", InfoRight, "RIGHT", -2, 0)

	CreateToggleButton("LeftToggleButton", "◄", InfoLeft, ChatBackgroundLeft, "Toggle Left Chat Panel")
	LeftToggleButton:Point("LEFT", InfoLeft, "LEFT", 2, 0)
end

SLASH_CHATTWEAKS1 = "/ct"
SlashCmdList["CHATTWEAKS"] = function()
	if ChatTweaksOptionsFrame:IsShown() then
		ChatTweaksOptionsFrame:Hide()
	else
		ChatTweaksOptionsFrame:Show()
	end
end